import ROOT
import os
import sys
import yaml
import logging
from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm

# Directory for logs
log_dir = "/afs/cern.ch/user/a/asehrawa/atlas-tutorial-asw/AnalysisTutorial/run/"
os.makedirs(log_dir, exist_ok=True)
log_file = os.path.join(log_dir, "job_submission.log")

# Setup logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s', filename=log_file, filemode='w')
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)

def debug_info(msg):
    logging.info(msg)

def debug_error(msg):
    logging.error(msg)

def makeSequence(configPath, dataType):
    # Read the YAML configuration file
    debug_info(f"Reading configuration from {configPath}")
    with open(configPath, 'r') as file:
        config = yaml.safe_load(file)

    # Create an empty algorithm sequence
    debug_info("Creating empty algorithm sequence...")
    algSeq = AlgSequence('AnalysisSequence')

    # Set up the systematics loader/handler service if specified in the config
    if 'CommonServices' in config:
        runSystematics = config['CommonServices'].get('runSystematics', True)
        debug_info(f"Systematics running: {runSystematics}")

    # Set up the output configuration if specified in the config
    if 'Output' in config:
        outputConfig = config['Output']
        debug_info(f"Output configuration: {outputConfig}")

        outputAlg = createAlgorithm('CP::TreeMakerAlg', 'TreeMaker')
        outputAlg.TreeName = outputConfig.get('treeName', 'analysis')
        algSeq += outputAlg

        #ntupleMaker = createAlgorithm('CP::AsgxAODNTupleMakerAlg', 'NTupleMaker')
        #ntupleMaker.TreeName = outputConfig.get('treeName', 'analysis')
        #ntupleMaker.Branches = outputConfig.get('containers', [])
        #debug_info(f"NtupleMaker branches: {ntupleMaker.Branches}")
        #algSeq += ntupleMaker

    debug_info(f"Algorithm sequence setup complete: {algSeq}")
    return algSeq

def main():
    try:
        # Set up the SampleHandler object to handle the input files
        debug_info("Setting up SampleHandler object...")
        sh = ROOT.SH.SampleHandler()

        # Set the name of the tree in our files in the xAOD the TTree containing the EDM containers is "CollectionTree"
        debug_info("Setting meta string 'nc_tree' to 'CollectionTree'...")
        sh.setMetaString('nc_tree', 'CollectionTree')

        # Create an algSequence from the YAML configuration file
        dataType = "mc"
        configPath = "/afs/cern.ch/user/a/asehrawa/atlas-tutorial-asw/AnalysisTutorial/source/MyAnalysis/data/config.yaml"
        debug_info(f"Data type: {dataType}")
        debug_info(f"Configuration path: {configPath}")

        # Properly call makeSequence with the required parameters
        debug_info("Creating algorithm sequence...")
        algSeq = makeSequence(configPath, dataType)
        debug_info(f"Algorithm sequence created: {algSeq}")

        # Select the sample associated with the data type used
        if dataType not in ["data", "mc"]:
            raise Exception(f"Invalid data type: {dataType}")

        if dataType == 'mc':
            testFile = '/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1'
        else:
            testFile = os.getenv('ASG_TEST_FILE_DATA')

        debug_info(f"Test file: {testFile}")

        # Use SampleHandler to get the sample from the defined location
        debug_info("Adding sample to SampleHandler...")
        sample = ROOT.SH.SampleLocal("dataset")
        sample.add(testFile)
        sh.add(sample)
        debug_info("SampleHandler setup complete")

        # Create an EventLoop job
        debug_info("Creating EventLoop job...")
        job = ROOT.EL.Job()
        job.sampleHandler(sh)
        debug_info("EventLoop job created")

        debug_info("Adding algorithm sequence to job...")
        algSeq.addSelfToJob(job)
        debug_info("Algorithm sequence added to job")

        # Add output stream
        debug_info("Adding output stream 'ANALYSIS'...")
        job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))
        debug_info("Output stream added")

        # Add some options for the job
        debug_info("Setting job options...")
        job.options().setDouble(ROOT.EL.Job.optMaxEvents, 500)
        job.options().setString(ROOT.EL.Job.optSubmitDirMode, 'unique-link')
        debug_info("Job options set")
        
        # Run the job using the direct driver
        driver = ROOT.EL.DirectDriver()
        submission_dir = "submitDir"
        debug_info(f"Submitting job to directory: {submission_dir}")
        driver.submit(job, submission_dir)
        debug_info("Job submitted")

        debug_info("Job submission complete. Check logs for details.")

    except Exception as e:
        debug_error(f"An error occurred: {e}")
        raise

if __name__ == "__main__":
    main()
