#!/usr/bin/env python

import ROOT
import cppyy
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.AlgSequence import AlgSequence
import sys
import datetime

def main():
    # Initialize xAOD
    if not ROOT.xAOD.Init().isSuccess():
        print("Failed to initialize xAOD")
        return

    # Create an instance of SH::SampleHandler
    sh = ROOT.SH.SampleHandler()

    # Specify the input file(s)
    filePath = "/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1"
    
    # Create a new sample object for the input file
    sample = ROOT.SH.SampleLocal("dataset")
    sample.add(filePath)
    sh.add(sample)

    # Print the content of the sample handler
    sh.printContent()

    # Create an algorithm sequence
    algSeq = AlgSequence("AnalysisSequence")

    # Create the algorithm and add it to the sequence
    alg = createAlgorithm("MyAnalysis::MyxAODAnalysis", "MyxAODAnalysis")
    algSeq += alg

    # Set up the job
    job = ROOT.EL.Job()
    job.sampleHandler(sh)

    # Add each algorithm in the sequence individually to the job
    for algorithm in algSeq:
        job.algsAdd(algorithm)

    # Create the driver for local execution
    driver = ROOT.EL.DirectDriver()

    # Create a unique submission directory using a timestamp
    timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    submission_dir = f"submitDir_{timestamp}"

    driver.submit(job, submission_dir)

if __name__ == "__main__":
    main()
