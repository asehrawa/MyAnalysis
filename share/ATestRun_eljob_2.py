#!/usr/bin/env python

# Import os to be able to read environment variables
import os

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config-path', dest='configPath',
        action='store', type=str,
        help='Path to the directory containing samples to be processed.')
parser.add_argument( '-s', '--submission-dir', dest='submitDir',
        action='store', type=str, default='submitDir',
        help='Submission directory for EventLoop')
options = parser.parse_args()

# data type: 'data', 'mc'
dataType = 'mc'

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the SampleHandler object to handle the input files
sh = ROOT.SH.SampleHandler()

# Set the name of the tree in our files in the xAOD the TTree
# containing the EDM containers is "CollectionTree"
sh.setMetaString( 'nc_tree', 'CollectionTree' )

# Select the sample associated with the data type used
if dataType not in ["data", "mc"] :
    raise Exception (f"invalid data type: {dataType}")
if dataType == 'mc':
    testFile = '/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_\
M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1'
    #testFile = os.getenv ('ALRB_TutorialData')+'/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1'
else:
    testFile = os.getenv('ASG_TEST_FILE_DATA')

# Use SampleHandler to get the sample from the defined location
sample = ROOT.SH.SampleLocal("dataset")
sample.add (testFile)
sh.add (sample)

# Print information about the sample
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# Add our algorithm to the job
job.algsAdd( alg )

# Later on we'll add some configuration options for our job here

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submitDir )

# Do not add anything 
