import ROOT
import os
import sys
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnalysisAlgorithmsConfig.ConfigAccumulator import DataType

sys.path.append(os.path.abspath('../source/MyAnalysis/python'))

# Set up the SampleHandler object to handle the input files
sh = ROOT.SH.SampleHandler()

# Set the name of the tree in our files in the xAOD the TTree containing the EDM containers is "CollectionTree"
sh.setMetaString('nc_tree', 'CollectionTree')

# Create an algSequence from the YAML configuration file
from MyAnalysisAlgorithms import makeSequence

dataType = "mc"
configPath = "/afs/cern.ch/user/a/asehrawa/atlas-tutorial-asw/AnalysisTutorial/source/MyAnalysis/data/config.yaml"

# Properly call makeSequence with the required parameters
algSeq = makeSequence(configPath, dataType)
print(algSeq)  # For debugging

# Select the sample associated with the data type used
if dataType not in ["data", "mc"]:
    raise Exception(f"invalid data type: {dataType}")
if dataType == 'mc':
    testFile = '/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1'
else:
    testFile = os.getenv('ASG_TEST_FILE_DATA')

# Use SampleHandler to get the sample from the defined location
sample = ROOT.SH.SampleLocal("dataset")
sample.add(testFile)
sh.add(sample)

# Create an EventLoop job
job = ROOT.EL.Job()
job.sampleHandler(sh)

algSeq.addSelfToJob(job)

# Add output stream
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Add some options for the job
job.options().setDouble(ROOT.EL.Job.optMaxEvents, 500)
job.options().setString(ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Run the job using the direct driver
driver = ROOT.EL.DirectDriver()
submission_dir = "submitDir"
driver.submit(job, submission_dir)
