# Select the sample associated with the data type used
if dataType not in ["data", "mc"] :
    raise Exception (f"invalid data type: {dataType}")
if dataType == 'mc':
    testFile = os.getenv ('ALRB_TutorialData')+'/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1'
else:
    testFile = os.getenv('ASG_TEST_FILE_DATA')

# Override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile]

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# Limit the number of events (for testing purposes)
theApp.EvtMax = 500

# Optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")


