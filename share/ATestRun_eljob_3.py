#!/usr/bin/env python

# Import required libraries
import os
import argparse
import ROOT

# Initialize ROOT
ROOT.xAOD.Init().ignore()

# Setup command line arguments
parser = argparse.ArgumentParser(description='Process some data samples.')
parser.add_argument('-c', '--config-path', dest='configPath', action='store', type=str,
                    help='Path to the directory containing samples to be processed.')
parser.add_argument('-s', '--submission-dir', dest='submitDir', action='store', type=str,
                    default='submitDir', help='Submission directory for EventLoop')
options = parser.parse_args()

# Define the data type
dataType = 'mc'

# Setup the SampleHandler to manage input files
sh = ROOT.SH.SampleHandler()
sh.setMetaString('nc_tree', 'CollectionTree')  # Name of the tree in our files

# Define the test file based on data type
if dataType == 'mc':
    testFile = '/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1'
else:
    testFile = os.getenv('ASG_TEST_FILE_DATA')

# Check if testFile is defined
if not testFile:
    raise Exception("Test file path is not defined for the given data type.")

# Add the test file to the sample handler
sample = ROOT.SH.SampleLocal("dataset")
sample.add(testFile)
sh.add(sample)

# Print information about the sample
sh.printContent()

# Create an EventLoop job
job = ROOT.EL.Job()
job.sampleHandler(sh)
job.options().setDouble(ROOT.EL.Job.optMaxEvents, 500)
job.options().setString(ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Configure the algorithm
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm('MyxAODAnalysis', 'AnalysisAlg')
job.algsAdd(alg)

# Run the job using the direct driver
driver = ROOT.EL.DirectDriver()
driver.submit(job, options.submitDir)

# End of script
