#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/PropertyWrapper.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

private:
  //TTree* m_tree = nullptr; // The TTree to output data
  //std::vector<float> electron_pt, electron_eta, electron_phi;
  //std::vector<float> muon_pt, muon_eta, muon_phi;
  //std::vector<float> jet_pt, jet_eta, jet_phi;
  //float met_et = 0.0, met_phi = 0.0;
  unsigned int m_runNumber = 0;
  unsigned long long m_eventNumber = 0;
};

#endif // MyAnalysis_MyxAODAnalysis_H














//#ifndef MyAnalysis_MyxAODAnalysis_H
//#define MyAnalysis_MyxAODAnalysis_H

//#include <AnaAlgorithm/AnaAlgorithm.h>
//#include <AsgTools/PropertyWrapper.h>
//#include <TH1.h>
//#include <TTree.h>
//#include <vector>

//class MyxAODAnalysis : public EL::AnaAlgorithm
//{
//public:
  // This is a standard algorithm constructor
  //MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // These are the functions inherited from Algorithm
  //virtual StatusCode initialize () override;
  //virtual StatusCode execute () override;
  //virtual StatusCode finalize () override;

//private:
  // Configuration, and any other types of variables go here.
  // Event identifier variables
  //unsigned int m_runNumber = 0; //< Run number
  //unsigned long long m_eventNumber = 0; //< Event number  
//};

//#endif // MyAnalysis_MyxAODAnalysis_H

