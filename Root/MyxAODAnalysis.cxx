#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <AsgMessaging/MessageCheck.h>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{}

StatusCode MyxAODAnalysis :: initialize ()
{
  //ANA_CHECK(book( TTree("analysis", "Result Tree") ));
  //m_tree = tree("analysis");

  //m_tree->Branch("electron_pt", &electron_pt);
  //m_tree->Branch("electron_eta", &electron_eta);
  //m_tree->Branch("electron_phi", &electron_phi);
  //m_tree->Branch("muon_pt", &muon_pt);
  //m_tree->Branch("muon_eta", &muon_eta);
  //m_tree->Branch("muon_phi", &muon_phi);
  //m_tree->Branch("jet_pt", &jet_pt);
  //m_tree->Branch("jet_eta", &jet_eta);
  //m_tree->Branch("jet_phi", &jet_phi);
  //m_tree->Branch("met_et", &met_et);
  //m_tree->Branch("met_phi", &met_phi);
  //m_tree->Branch("runNumber", &m_runNumber);
  //m_tree->Branch("eventNumber", &m_eventNumber);

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    m_runNumber = eventInfo->runNumber();
    m_eventNumber = eventInfo->eventNumber();

    //const xAOD::ElectronContainer* electrons = nullptr;
    //ANA_CHECK(evtStore()->retrieve(electrons, "Electrons"));
    //const xAOD::MuonContainer* muons = nullptr;
    //ANA_CHECK(evtStore()->retrieve(muons, "Muons"));
    //const xAOD::JetContainer* jets = nullptr;
    //ANA_CHECK(evtStore()->retrieve(jets, "AntiKt4EMTopoJets"));
    //const xAOD::MissingETContainer* met = nullptr;
    //ANA_CHECK(evtStore()->retrieve(met, "MET_Core_AntiKt4EMPFlow"));

    //electron_pt.clear();
    //electron_eta.clear();
    //electron_phi.clear();
    //for (const xAOD::Electron* electron : *electrons) {
    //  electron_pt.push_back(electron->pt());
    //  electron_eta.push_back(electron->eta());
    //  electron_phi.push_back(electron->phi());
    //}

    //muon_pt.clear();
    //muon_eta.clear();
    //muon_phi.clear();
    //for (const xAOD::Muon* muon : *muons) {
    //  muon_pt.push_back(muon->pt());
    //  muon_eta.push_back(muon->eta());
    //  muon_phi.push_back(muon->phi());
    //}

    //jet_pt.clear();
    //jet_eta.clear();
    //jet_phi.clear();
    //for (const xAOD::Jet* jet : *jets) {
    //  jet_pt.push_back(jet->pt());
    //  jet_eta.push_back(jet->eta());
    //  jet_phi.push_back(jet-> phi());
    //}

    //met_et = met->at(0)->met(); 
    //met_phi = met->at(0)->phi();

    //m_tree->Fill();

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: finalize ()
{
    ANA_MSG_INFO ("Finalizing");
    return StatusCode::SUCCESS;
}





























/**
#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <TFile.h>
#include <TTree.h>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. This is also where you
  // declare all properties for your algorithm. Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}

StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees. This method gets called before any input files are
  // connected.

  // Create output TTree
 
  ANA_MSG_INFO ("in initialize");

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // event, e.g. read input variables, apply cuts, and fill
  // histograms and trees. This is where most of your actual analysis
  // code will go.

  // Retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  if (!evtStore()->retrieve(eventInfo, "EventInfo").isSuccess()) {
    ANA_MSG_ERROR("Failed to retrieve EventInfo");
    return StatusCode::FAILURE;
  }
  ANA_MSG_INFO("Successfully retrieved EventInfo");

  // Access run number and event number and store as local variables
  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
 
  // Print out the values for debugging
  ANA_MSG_INFO("runNumber = " << m_runNumber);
  ANA_MSG_INFO("eventNumber = " << m_eventNumber);
 
  ANA_MSG_INFO ("in execute");

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // The output file will be managed by EventLoop, so we do not need to explicitly open and close the file here.

  return StatusCode::SUCCESS;
}

**/
