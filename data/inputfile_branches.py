import ROOT

# Path to your ROOT file
file_path = "/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024/mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PH\
YS.e7587_e7400_a907_r14861_r14919_p6026/DAOD_PHYS.37773721._000001.pool.root.1"

# Open the ROOT file
file = ROOT.TFile.Open(file_path)

# List all keys (top-level objects) in the file
keys = file.GetListOfKeys()
for key in keys:
    obj_name = key.GetName()
    obj = file.Get(obj_name)
    obj_type = obj.ClassName()
    print(f"Object: {obj_name} (Type: {obj_type})")
    
    # If the object is a tree, list all branches
    if obj.InheritsFrom("TTree"):
        print(f"Tree: {obj_name}")
        branches = obj.GetListOfBranches()
        for branch in branches:
            print(f"  Branch: {branch.GetName()}")

# Close the ROOT file
file.Close()
